import { FSComponentLoader, Plugin, PluginAPI } from '@ayanaware/bento';

import { SnoowrapOptions } from 'snoowrap';

export class Bentowrap implements Plugin {
  public name = 'Bentowrap';
  public api!: PluginAPI;

  public snoowrapOptions: SnoowrapOptions;
  
  public fsLoader: FSComponentLoader;

  public constructor(snoowrapOptions: SnoowrapOptions) {
    this.snoowrapOptions = snoowrapOptions;
  }

  public async onLoad() {
    this.fsLoader = new FSComponentLoader();
    this.fsLoader.name = 'BentowrapFSComponentLoader';
    await this.api.bento.addPlugin(this.fsLoader);

    return this.api.loadComponents(this.fsLoader, __dirname, 'components');
  }
}