import { Component, ComponentAPI, PluginReference } from '@ayanaware/bento';

import { Bentowrap } from '../Bentowrap';
import * as swrap from 'snoowrap';

export class Snoowrap implements Component {
  public name = 'Snoowrap';
  public api!: ComponentAPI;
  public parent: PluginReference = Bentowrap;

  public client: swrap;

  public async onLoad() {
    this.client = new swrap(this.api.getEntity(this.parent)['snoowrapOptions']);
  }
}

